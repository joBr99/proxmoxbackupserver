# docker-proxmoxbackupserver

Docker image for Proxmox Backup Server (https://pbs.proxmox.com).

Run with:
```
docker run -it \
      -p 8007:8007 \
      -v /config_directory:/etc/proxmox-backup \
      -v /backup_directory:/backup \
      -v /etc/timezone:/etc/timezone:ro
      -e ADMIN_PASSWORD=*ADMIN_USER_PASSWORD*
      supsy/proxmoxbackupserver
```

with admin password generation:
```
docker run -it \
      -p 8007:8007 \
      -v /config_directory:/etc/proxmox-backup \
      -v /backup_directory:/backup \
      -v /etc/timezone:/etc/timezone:ro
      -e RANDOM_ADMIN_PASSWORD=yes
      supsy/proxmoxbackupserver
```

Or with docker-compose:
```
pbs:
  image: supsy/proxmoxbackupserver
  ports:
    - "8007:8007"     # http frontend
  volumes:
    - /config_directory:/etc/proxmox-backup
    - /backup_directory:/backup
    - /etc/timezone:/etc/timezone:ro
  environment:
    - ADMIN_PASSWORD=*ADMIN_USER_PASSWORD*
  restart: unless-stopped
```


The backup directory must have the proper permissions. Otherwise pbs will not work. You can set it with:
 ```
 mkdir -p /backup_directory
 chown 34:65534 /backup_directory
 ```

The config directory must have the proper permissions. Otherwise pbs will not start. You can set it with:
 ```
 mkdir -p /config_directory
 chown 34:34 /config_directory
 chmod 700 /config_directory
 ```

After start the webinterface is available under https://*docker*:8007
* Username: admin
* Realm: Proxmox Backup authentication server (Must be explicitly changed on first login)
* Password: *asDefinedViaEnvironment*

**Hint** The user admin permissions are limited to reflect docker limitations.
         The ADMIN_PASSWORD or RANDOM_ADMIN_PASSWORD is only needed for first time initialization.
